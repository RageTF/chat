package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Rage on 13.12.2016.
 */
public abstract class Server extends Thread implements ServerInterface {

    public final static int PORT = 4545;

    protected ServerSocket server;
    protected boolean isStart;

    public Server(int port) throws IOException {
        server = new ServerSocket(port);
    }

    public void startServer() {
        isStart = true;
        if (!isAlive()) {
            start();
        }
    }

    public void stopServer() throws IOException {
        isStart = false;
        if (!server.isClosed()) {
            server.close();
        }
    }

    @Override
    public void run() {

        while (isStart) {
            try {
                System.out.println("Wait connect!");
                Socket connectSocket = server.accept();
                System.out.println("Connected!");
                serverActions(connectSocket);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}

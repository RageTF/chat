package server;

import java.net.Socket;

/**
 * Created by Rage on 13.12.2016.
 */
public interface ServerInterface {

    /*

    Метод описывает действия, которые должен осуществлять сервер, когда к нему подключился новый клиент.

     */

    void serverActions(Socket connectSocket);

}

package chat.gui;

import chat.server.ChatServer;
import chat.server.user.ConnectionInterface;
import chat.server.user.User;
import server.Server;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.BindException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

/**
 * Created by Rage on 15.12.2016.
 */
public class Autorization extends JFrame implements ConnectionInterface, ActionListener {

    private JLabel labelLogin;
    private JTextField login;
    private JButton enter;

    private JLabel labelHost;
    private JTextField host;

    private JButton startServer;

    private User user;
    private ChatServer chatServer;

    public Autorization() {

        JPanel hostPanel = new JPanel();

        labelHost = new JLabel("Host: ");
        labelHost.setHorizontalAlignment(SwingConstants.CENTER);
        labelHost.setVerticalAlignment(SwingConstants.CENTER);
        labelHost.setPreferredSize(new Dimension(100, 50));

        host = new JTextField();
        host.setHorizontalAlignment(SwingConstants.CENTER);
        host.setPreferredSize(new Dimension(300, 50));

        hostPanel.add(labelHost);
        hostPanel.add(host);

        JPanel loginPanel = new JPanel();

        labelLogin = new JLabel("Login: ");
        labelLogin.setPreferredSize(new Dimension(100, 50));
        labelLogin.setHorizontalAlignment(SwingConstants.CENTER);
        labelLogin.setVerticalAlignment(SwingConstants.CENTER);

        login = new JTextField();
        login.setPreferredSize(new Dimension(200, 50));
        login.setHorizontalAlignment(SwingConstants.CENTER);

        enter = new JButton("Enter");
        enter.setPreferredSize(new Dimension(100, 50));
        enter.setHorizontalAlignment(SwingConstants.CENTER);
        enter.setVerticalAlignment(SwingConstants.CENTER);
        enter.addActionListener(this);

        loginPanel.add(labelLogin);
        loginPanel.add(login);
        loginPanel.add(enter);

        startServer = new JButton("Start server");
        startServer.addActionListener(this);

        setLayout(new GridLayout(3, 1));

        add(hostPanel);
        add(loginPanel);
        add(startServer);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
        setVisible(true);


    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == enter) {

            String addressHost = host.getText();
            String loginText = login.getText();

            InetAddress inetAddress = null;

            try {
                inetAddress = InetAddress.getByName(addressHost);
            } catch (UnknownHostException e1) {
                e1.printStackTrace();
                JOptionPane.showMessageDialog(this, "Incorrect host!");
                host.setText("");
                return;
            }

            if (!"".equals(loginText.replace(" ", "")) && loginText == null) {
                JOptionPane.showMessageDialog(this, "Incorrect login!");
                return;
            }

            try {
                if (user == null || (user != null && !Arrays.equals(user.getSocketAddress().getAddress(), inetAddress.getAddress()))) {
                    user = new User(InetAddress.getByName(addressHost), Server.PORT);
                }

                user.setConnectionInterface(this);
                user.confirmLogin(loginText);

            } catch (ConnectException e1) {
                JOptionPane.showMessageDialog(this, "Error connect!");
                user=null;
            } catch (IOException e1) {
                e1.printStackTrace();
                return;
            }
        } else if (e.getSource() == startServer) {
            try {
                chatServer = new ChatServer(ChatServer.PORT);
                chatServer.startServer();
                host.setText(InetAddress.getLocalHost().getHostAddress());
                JOptionPane.showMessageDialog(this, "Server start!");
            }catch (BindException e1){
                e1.printStackTrace();
                JOptionPane.showMessageDialog(this,"Server has already worked!");
            } catch (UnknownHostException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public void initUsers(ArrayList<String> users) {
        dispose();
        new MainFrame(user, users);
    }

    public void loginConfirm(String login) {

    }

    public void loginNotConfirm(String login) {
        JOptionPane.showMessageDialog(this, "Login already used.");
    }

    public static void main(String[] args) {
        new Autorization();
    }
}

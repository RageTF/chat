package chat.gui;

import chat.server.data.Message;
import chat.server.user.User;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Rage on 14.12.2016.
 */
public class OnlineUserPanel extends JPanel implements ListSelectionListener {

    public final static String USER_GENERAL_CHAT = "General chat";

    private JLabel label;

    private int selected;

    private ArrayList<String> users;
    private ArrayList<Integer> countUnReadMessage;
    private DefaultListModel defaultListModel;
    private JScrollPane listScroll;
    private JList listOnlineUsers;

    private User user;
    private ChatPanel chatPanel;

    public OnlineUserPanel(ChatPanel chatPanel, User user, ArrayList<String> users) {

        countUnReadMessage=new ArrayList<>();

        this.chatPanel = chatPanel;
        this.user = user;
        this.users = users;

        label = new JLabel("ONLINE");
        label.setPreferredSize(new Dimension(250, 25));
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setVerticalAlignment(SwingConstants.CENTER);

        defaultListModel = new DefaultListModel();

        for (String onlineUser : users) {
            if (onlineUser == null) {
                defaultListModel.addElement(USER_GENERAL_CHAT);
            } else {
                defaultListModel.addElement(onlineUser);
            }
            countUnReadMessage.add(0);
        }

        listOnlineUsers = new JList(defaultListModel);
        listOnlineUsers.addListSelectionListener(this);
        listOnlineUsers.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        selected = users.indexOf(USER_GENERAL_CHAT);
        listOnlineUsers.setSelectedIndex(selected);
        chatPanel.showMessages(null);

        listScroll = new JScrollPane(listOnlineUsers);
        listScroll.setPreferredSize(new Dimension(250, 525));

        setLayout(new BorderLayout());

        add(label, BorderLayout.NORTH);
        add(listScroll, BorderLayout.CENTER);
    }

    public void addNewUser(String login) {
        if (!login.equals(user.getLogin())) {
            users.add(login);
            countUnReadMessage.add(0);
            defaultListModel.addElement(login);
        }
    }

    public void disconnectUser(String login) {
        int index=users.indexOf(login);
        users.remove(index);
        countUnReadMessage.remove(index);
        if(index==selected){
            if(index>0){
                listOnlineUsers.setSelectedIndex(index-1);
            }else{
                listOnlineUsers.setSelectedIndex(index+1);
            }
        }
        defaultListModel.removeElementAt(index);
    }

    public void valueChanged(ListSelectionEvent e) {
        selected = listOnlineUsers.getSelectedIndex();
        if (selected < 0 || selected > users.size()) {
            selected = users.indexOf(null);
        }
        countUnReadMessage.set(selected,0);
        /*
        Помечаем сообщения выбранного пользователя , как прочитанные.
         */
        if(users.get(selected)==null){
            defaultListModel.setElementAt(USER_GENERAL_CHAT,selected);
        }else{
            defaultListModel.setElementAt(users.get(selected).toString(),selected);
        }
        chatPanel.showMessages(users.get(selected));
    }

    /*
    Отображает не прочитанные сообщения и их количество.
     */
    public void receive(Message message){

        String senderLogin=message.getSenderLogin();
        String receiveLogin=message.getReceiverLogin();

        int index=users.indexOf(senderLogin);
        int unReadUser=countUnReadMessage.get(index)+1;

        if(chatPanel.getViewLogin()==null){
            if(receiveLogin!=null){
                countUnReadMessage.set(index,unReadUser);
                defaultListModel.setElementAt(users.get(index)+" ("+unReadUser+")",index);
                notification();
            }
        }else{
            if(receiveLogin==null || !senderLogin.equals(chatPanel.getViewLogin())){
                countUnReadMessage.set(index,unReadUser);
                defaultListModel.setElementAt(users.get(index)+" ("+unReadUser+")",index);
                notification();
            }
        }
    }

    /*
    Звуковое оповещение.
     */
    private void notification() {
        try {
            BufferedInputStream in = new BufferedInputStream(getClass().getClassLoader().getResourceAsStream("sounds/notice.au"));
            AudioStream audioStream = new AudioStream(in);
            AudioPlayer.player.start(audioStream);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}

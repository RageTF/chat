package chat.gui;

import chat.server.data.Message;
import chat.server.user.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

/**
 * Created by Rage on 14.12.2016.
 */
public class ChatPanel extends JPanel implements ActionListener {

    //Поле указывает login клиента, сообщения которого отображаются на данный момент в ChatPanel.
    private String viewLogin = null;

    private JLabel label;

    private JTextArea inputMessage;
    private JButton send;

    private DefaultListModel defaultListModel;
    private JScrollPane listScroll;
    private JList listMessages;

    private User user;

    public ChatPanel(User user) {

        this.user = user;

        label = new JLabel(OnlineUserPanel.USER_GENERAL_CHAT);
        label.setPreferredSize(new Dimension(500, 25));
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setVerticalAlignment(SwingConstants.CENTER);

        defaultListModel = new DefaultListModel();

        listMessages = new JList(defaultListModel);

        listScroll = new JScrollPane(listMessages);
        listScroll.setPreferredSize(new Dimension(500, 500));

        JPanel sendMessagesPanel = new JPanel();

        inputMessage = new JTextArea();
        inputMessage.setPreferredSize(new Dimension(500, 50));
        inputMessage.setLineWrap(true);

        send = new JButton("Send");
        send.setPreferredSize(new Dimension(100, 50));
        send.addActionListener(this);

        sendMessagesPanel.add(inputMessage);
        sendMessagesPanel.add(send);

        setLayout(new BorderLayout());
        add(label, BorderLayout.NORTH);
        add(listScroll, BorderLayout.CENTER);
        add(sendMessagesPanel, BorderLayout.SOUTH);
    }

    /*
    Отображает все сообщения отправленние и пришедшие от login.
     */
    public void showMessages(String login) {
        this.viewLogin = login;
        if (login != null) {
            label.setText(login);
        } else {
            label.setText(OnlineUserPanel.USER_GENERAL_CHAT);
        }
        defaultListModel.clear();
        for (Message message : user.getMessagesHistory().get(login)) {
            addMessage(message);
        }
    }

    /*
    Добавляет сообщение в спислк отображаемых.
     */
    public void addMessage(Message message) {
        defaultListModel.addElement(message.getSenderLogin() + ":  " + message.getMessageText());
    }

    public void actionPerformed(ActionEvent e) {

        if (send == e.getSource()) {
            try {
                if (!"".equals(inputMessage.getText().replace("\n", ""))) {
                    user.sendMessage(viewLogin, inputMessage.getText());
                    inputMessage.setText("");
                    inputMessage.requestFocusInWindow();
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }


    public String getViewLogin() {
        return viewLogin;
    }
}

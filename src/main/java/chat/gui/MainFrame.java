package chat.gui;

import chat.server.data.Message;
import chat.server.user.User;
import chat.server.user.UserInterface;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Rage on 15.12.2016.
 */
public class MainFrame extends JFrame implements UserInterface {

    private OnlineUserPanel onlineUsersPanel;
    private ChatPanel chatPanel;

    private User user;

    public MainFrame(User user, ArrayList<String> users) {
        super("Login: " + user.getLogin());
        this.user = user;
        user.setUserInterface(this);

        chatPanel = new ChatPanel(user);
        onlineUsersPanel = new OnlineUserPanel(chatPanel, user, users);
        setLayout(new BorderLayout());
        add(onlineUsersPanel, BorderLayout.WEST);
        add(chatPanel, BorderLayout.EAST);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
        setResizable(false);

    }

    @Override
    public void userDisconnectOrNotExist(String login) {
        JOptionPane.showMessageDialog(this, "Message is lost or " + login + " is offline");
    }

    @Override
    public void receiveMessage(Message message) {

        String sender = message.getSenderLogin();
        String receiver = message.getReceiverLogin();
        String viewLoginOnChatPanel = chatPanel.getViewLogin();

        if(!user.getLogin().equals(sender)){
            onlineUsersPanel.receive(message);
        }

        if (receiver == null) {

            if (viewLoginOnChatPanel == null) {
                chatPanel.addMessage(message);
            }

        } else if (receiver.equals(user.getLogin())) {
            if (sender.equals(chatPanel.getViewLogin())) {
                chatPanel.addMessage(message);
            }
        } else if (sender.equals(user.getLogin())) {
            if (receiver.equals(chatPanel.getViewLogin())) {
                chatPanel.addMessage(message);
            }
        }
    }

    @Override
    public void connectNewUser(String login) {
        onlineUsersPanel.addNewUser(login);
    }

    @Override
    public void disconnectUser(String login) {
        System.out.println("disconnect");
        onlineUsersPanel.disconnectUser(login);
    }

    @Override
    public void errorConnection() {
        dispose();
        JOptionPane.showMessageDialog(new Autorization(),"Server doesn't work!");
    }
}

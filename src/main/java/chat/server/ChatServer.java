package chat.server;

import chat.server.data.*;
import chat.server.data.Error;
import server.Server;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;

/**
 * Created by Rage on 13.12.2016.
 */
public class ChatServer extends Server {

    //Список подключенных, но не авторизованнх клиентов.
    private LinkedList<UserThread> anonymousUser;
    private HashMap<String, UserThread> users;

    public ChatServer(int port) throws IOException {
        super(port);
        anonymousUser = new LinkedList<UserThread>();
        users = new HashMap<String, UserThread>();
    }

    public void serverActions(Socket connectSocket) {
        UserThread userThread = new UserThread(connectSocket);
        anonymousUser.add(userThread);
    }

    /*
    Отправляем Parcel всем клиентам.
     */
    public void sendBroadcast(Parcel parcel) {
        for (HashMap.Entry<String, UserThread> userThreadEntry : users.entrySet()) {
            UserThread userThread = userThreadEntry.getValue();
            try {
                userThread.objectOutputStream.writeObject(parcel);
                userThread.objectOutputStream.reset();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class UserThread extends Thread {

        Socket userSocket;
        String login;

        ObjectInputStream objectInputStream;
        ObjectOutputStream objectOutputStream;

        UserThread(Socket userSocket) {
            this.userSocket = userSocket;

            try {
                objectInputStream = new ObjectInputStream(userSocket.getInputStream());
                objectOutputStream = new ObjectOutputStream(userSocket.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
            start();
        }

        public void deliverMessage(Message message) throws IOException {
            objectOutputStream.writeObject(message);
            objectOutputStream.reset();
        }

        @Override
        public void run() {
            while (!userSocket.isClosed()) {
                try {
                    Object inputObject = objectInputStream.readObject();

                    if (inputObject instanceof AuthorizationForm) {

                        AuthorizationForm authorizationForm = (AuthorizationForm) inputObject;
                        String login = authorizationForm.getLogin();

                        if (!users.containsKey(login)) {
                            this.login = login;
                            users.put(login, this);
                            anonymousUser.remove(this);
                            ArrayList<String> arrayList = new ArrayList<String>(users.keySet());
                            arrayList.remove(login);
                            objectOutputStream.writeObject(new Info(Info.INFO_LOGIN_CONFIRM, login));
                            objectOutputStream.reset();
                            objectOutputStream.writeObject(new Info(Info.INFO_USERS_COLLECTION, arrayList));
                            objectOutputStream.reset();
                            sendBroadcast(new Info(Info.INFO_CONNECT_USER, login));
                        } else {
                            objectOutputStream.writeObject(new Error(Error.ERROR_LOGIN_ALREADY_USED, login));
                            objectOutputStream.reset();
                        }
                    } else if (inputObject instanceof Message) {

                        Message message = (Message) inputObject;
                        String receiverLogin = message.getReceiverLogin();

                        if (receiverLogin == null) {
                            sendBroadcast(message);
                            continue;
                        }

                        if (users.containsKey(receiverLogin)) {
                            UserThread receiverSocket = users.get(receiverLogin);
                            receiverSocket.deliverMessage(message);
                            deliverMessage(message);
                        } else {
                            objectOutputStream.writeObject(new Info(Info.INFO_RECEIVER_OFFLINE_OR_NOT_EXIST, receiverLogin));
                            objectOutputStream.reset();
                        }

                    } else if (inputObject instanceof Error) {

                    } else if (inputObject instanceof Info) {

                    }
                } catch (EOFException|SocketException e) {
                    e.printStackTrace();
                    if (users.containsKey(login)) {
                        users.remove(login);
                        sendBroadcast(new Info(Info.INFO_DISCONNECT_USER, login));
                    } else {
                        anonymousUser.remove(this);
                    }
                    try {
                        userSocket.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

            }
        }
    }

}

package chat.server.user;

import java.util.ArrayList;

/**
 * Created by Rage on 15.12.2016.
 */

/*
Интерфейс для взаимодействия с процессом подключения к сервера.
 */

public interface ConnectionInterface {
    /*
    Метод описывает действия, которые будут выполнены сразу после успешного подключения.
    Сюда придет список всех уже подключенных к серверу клиентов.
     */
    void initUsers(ArrayList<String> users);
    /*
    Метод описывает действия, которые будут выполнены при успешном подключении.
     */
    void loginConfirm(String login);
    /*
    Метод описывает действия, которые будут выполнены при неудачном подключении.
     */
    void loginNotConfirm(String login);
}

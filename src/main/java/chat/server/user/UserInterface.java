package chat.server.user;

import chat.server.data.Message;
import java.util.Set;

/**
 * Created by Rage on 13.12.2016.
 */

/*
Интерфейс для взаимодействия с данными, которые пришли от сервера.
 */

public interface UserInterface{
    /*
    Метод описывает действия, которые будут выполнены если отправленное сообщение не дошло до пользователя.
     */
    void userDisconnectOrNotExist(String login);

    /*
    Метод описывает действия, которые будут выполнены при получении нового сообщения.
     */
    void receiveMessage(Message message);
    /*
    Метод описывает действия, которые будут выполнены при продключении нового пользователя.
     */
    void connectNewUser(String login);
    /*
    Метод описывает действия, которые будут выполнены при отключении пользователя.
     */
    void disconnectUser(String login);
    /*
    Метод описывает действия, которые будут выполнены при отсутствии подключения к серверу.
     */
    void errorConnection();
}

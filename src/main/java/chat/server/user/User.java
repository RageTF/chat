package chat.server.user;

import chat.server.ChatServer;
import chat.server.data.AuthorizationForm;
import chat.server.data.Error;
import chat.server.data.Info;
import chat.server.data.Message;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by Rage on 13.12.2016.
 */
public class User extends Thread {

    private String login;
    private Socket userSocket;
    private UserInterface userInterface;
    private ConnectionInterface connectionInterface;
    private HashMap<String, ArrayList<Message>> messagesHistory;
    private ObjectInputStream objectInputStream;
    private ObjectOutputStream objectOutputStream;

    public User(InetAddress serverAddress, int port) throws IOException {
        userSocket = new Socket(serverAddress, port);
        System.out.println(userSocket.getInetAddress().getHostAddress());
        System.out.print("Connect");
        messagesHistory = new HashMap<String, ArrayList<Message>>();
        objectOutputStream = new ObjectOutputStream(userSocket.getOutputStream());
        start();
    }

    public void setUserInterface(UserInterface userInterface) {
        this.userInterface = userInterface;
    }

    public void setConnectionInterface(ConnectionInterface connectionInterface) {
        this.connectionInterface = connectionInterface;
    }

    @Override
    public void run() {

        try {
            objectInputStream = new ObjectInputStream(userSocket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (!userSocket.isClosed()) {
            try {
                Object inputObject = objectInputStream.readObject();
                if (inputObject instanceof Message) {

                    /*
                    Сюда будут приходить сообщения от других клиентов,в том числе и от нашего.
                     */

                    Message message = (Message) inputObject;
                    if (message.getReceiverLogin() == null) {
                        messagesHistory.get(null).add(message);
                        userInterface.receiveMessage(message);
                    } else if (message.getReceiverLogin().equals(login)) {
                        String senderLogin = message.getSenderLogin();
                        messagesHistory.get(senderLogin).add(message);
                        userInterface.receiveMessage(message);
                    } else if(message.getSenderLogin().equals(login)){
                        messagesHistory.get(message.getReceiverLogin()).add(message);
                        userInterface.receiveMessage(message);
                    }

                } else if (inputObject instanceof Error) {
                    Error error = (Error) inputObject;
                    if (error.getCodeError() == Error.ERROR_LOGIN_ALREADY_USED) {
                        connectionInterface.loginNotConfirm(error.getResponse());
                    }
                } else if (inputObject instanceof Info) {
                    Info info = (Info) inputObject;
                    if (info.getCodeInfo() == Info.INFO_LOGIN_CONFIRM) {
                        this.login = info.getResponse();
                        connectionInterface.loginConfirm(info.getResponse());
                    } else if (info.getCodeInfo() == Info.INFO_RECEIVER_OFFLINE_OR_NOT_EXIST) {
                        messagesHistory.remove(info.getResponse());
                        userInterface.userDisconnectOrNotExist(info.getResponse());
                    } else if (info.getCodeInfo() == Info.INFO_USERS_COLLECTION) {
                        ArrayList<String> users = info.getData();
                        users.add(null);
                        for (String login : users) {
                            if (!this.login.equals(login)) {
                                messagesHistory.put(login, new ArrayList<Message>());
                            }
                        }
                        connectionInterface.initUsers(users);
                    } else if (info.getCodeInfo() == Info.INFO_CONNECT_USER) {
                        messagesHistory.put(info.getResponse(), new ArrayList<Message>());
                        userInterface.connectNewUser(info.getResponse());
                    } else if (info.getCodeInfo() == Info.INFO_DISCONNECT_USER) {
                        messagesHistory.remove(info.getResponse());
                        userInterface.disconnectUser(info.getResponse());
                    }

                }

            } catch (EOFException|SocketException e) {
                e.printStackTrace();
                try {
                    userSocket.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                userInterface.errorConnection();
                break;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendMessage(String receiver, String text) throws IOException {
        Message message = new Message(login, receiver, text);
        if (message != null) {
            objectOutputStream.writeObject(message);
            objectOutputStream.flush();
            objectOutputStream.reset();
        }
    }

    public void confirmLogin(String login) throws IOException {
        objectOutputStream.writeObject(new AuthorizationForm(login));
        objectOutputStream.flush();
        objectOutputStream.reset();

    }

    public String getLogin() {
        return login;
    }

    public HashMap<String, ArrayList<Message>> getMessagesHistory() {
        return messagesHistory;
    }

    public InetAddress getSocketAddress(){
        return userSocket.getInetAddress();
    }
}

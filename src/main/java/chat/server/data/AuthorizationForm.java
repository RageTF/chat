package chat.server.data;

import java.io.Serializable;

/**
 * Created by Rage on 13.12.2016.
 */

/*
Форма авторизации пользователя на сервере.
 */
public class AuthorizationForm extends Parcel implements Serializable {

    private String login;

    public AuthorizationForm(String login){
        this.login=login;
    }

    public String getLogin() {
        return login;
    }
}

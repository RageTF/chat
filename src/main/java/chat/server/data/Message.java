package chat.server.data;

import java.io.Serializable;

/**
 * Created by Rage on 13.12.2016.
 */

public class Message extends Parcel implements Serializable{

    private String senderLogin;
    // Если в receiveLogin указать null, то сообщение отправится в общий чат.
    private String receiverLogin;
    private String message;

    public Message(String senderLogin,String receiverLogin,String message){
        this.senderLogin=senderLogin;
        this.receiverLogin=receiverLogin;
        this.message=message;
    }

    public String getSenderLogin() {
        return senderLogin;
    }

    public String getReceiverLogin() {
        return receiverLogin;
    }

    public String getMessageText() {
        return message;
    }
}

package chat.server.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

/**
 * Created by Rage on 13.12.2016.
 */

/*
Оповещения.
 */

public class Info extends Parcel implements Serializable {

    /*
    Коды оповещения.
     */

    //Код подтверждения регистрации на сервере
    public final static int INFO_LOGIN_CONFIRM=1;
    //Код неудачной доставки сообщения.
    public final static int INFO_RECEIVER_OFFLINE_OR_NOT_EXIST=2;
    //Код оповещения в котором содержаться данные о подключенных клиентах.
    public final static int INFO_USERS_COLLECTION=3;
    //Код оповещения о подключении нового пользователя.
    public final static int INFO_CONNECT_USER=4;
    //Код оповещения об отключении пользователя.
    public final static int INFO_DISCONNECT_USER=5;

    private int codeInfo;
    private String response;
    private ArrayList<String> data;

    public Info(int codeInfo,String response){
        this.codeInfo=codeInfo;
        this.response=response;
    }

    public Info(int codeInfo, ArrayList<String> data){
        this.codeInfo=codeInfo;
        this.data=data;
    }

    public int getCodeInfo(){
        return codeInfo;
    }

    public String getResponse(){
        return response;
    }

    public ArrayList<String> getData(){
        return data;
    }
}

package chat.server.data;

import java.io.Serializable;

/**
 * Created by Rage on 13.12.2016.
 */
public class Error extends Parcel implements Serializable {

    //Код ошибки о занятости используемого логина.
    public final static int ERROR_LOGIN_ALREADY_USED=1;

    private int codeError;
    private String response;

    public Error(int codeError,String response){
        this.codeError=codeError;
        this.response=response;
    }

    public int getCodeError() {
        return codeError;
    }

    public String getResponse() {
        return response;
    }
}
